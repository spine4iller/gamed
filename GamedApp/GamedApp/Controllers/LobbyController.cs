﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Service;
using Service.Models;

namespace GamedApp.Controllers
{
    [Authorize]
    public class LobbyController : Controller
    {
        // GET: Lobby
        public ActionResult Index()
        {
            var mdl = GameService.getUsersOnline();
            return View("Lobby",mdl);
        }        

        [HttpPost]
        public ActionResult Create(GameParams mdl)
        {
            mdl.State = GameState.Waiting;
            GameService.createGame(mdl);
            return RedirectToAction("GameStarted",mdl);
        }
        public ActionResult GameStarted(GameParams mdl)
        {
            return View(mdl);
        }
        public ActionResult Create()
        {
            var mdl = new GameParams { Players = GameType.Players2 };
            return View(mdl);
        }
        public ActionResult Connect(string game)
        {
           var result = GameService.newPlayerEntered(game,User.Identity.Name);
            if (result == EntranceResult.Start || result == EntranceResult.Wait)
            {
                var mdl = (result == EntranceResult.Start) ? new GameParams { State = GameState.Started } 
                    : new GameParams { State = GameState.Waiting };
                return RedirectToAction("GameStarted",mdl);
            }
            return RedirectToAction("Index");
            
        }
        public ActionResult Leave()
        {
            GameService.markPlayerLeaved(User.Identity.Name);
            return RedirectToAction("Index");
        }
    }
}