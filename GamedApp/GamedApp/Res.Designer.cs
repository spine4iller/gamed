﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace GamedApp {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    public class Res {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal Res() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("GamedApp.Res", typeof(Res).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Создать игру на двоих.
        /// </summary>
        public static string create2 {
            get {
                return ResourceManager.GetString("create2", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Создать новую игру .
        /// </summary>
        public static string createGame {
            get {
                return ResourceManager.GetString("createGame", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Эл. почта:.
        /// </summary>
        public static string Email {
            get {
                return ResourceManager.GetString("Email", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Войти.
        /// </summary>
        public static string enter {
            get {
                return ResourceManager.GetString("enter", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Привет, .
        /// </summary>
        public static string hello {
            get {
                return ResourceManager.GetString("hello", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Главная.
        /// </summary>
        public static string home {
            get {
                return ResourceManager.GetString("home", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Пароль:.
        /// </summary>
        public static string Password {
            get {
                return ResourceManager.GetString("Password", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Покинуть игру.
        /// </summary>
        public static string quit {
            get {
                return ResourceManager.GetString("quit", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Зарегистрироваться.
        /// </summary>
        public static string register {
            get {
                return ResourceManager.GetString("register", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Поле обязательное.
        /// </summary>
        public static string required {
            get {
                return ResourceManager.GetString("required", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Добро пожаловать.
        /// </summary>
        public static string welcome {
            get {
                return ResourceManager.GetString("welcome", resourceCulture);
            }
        }
    }
}
