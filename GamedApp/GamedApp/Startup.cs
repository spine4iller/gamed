﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(GamedApp.Startup))]
namespace GamedApp
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
            app.MapSignalR();
        }
    }
}
