﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.SignalR;
using Service;
using Service.Models;

namespace GamedApp.Hubs
{
    public class LobbyHub : Hub
    {
        public void Online(string user,bool offline)
        {
            Clients.All.online(user,offline);
        }
        public void iAmOnline(string user)
        {
            GameService.onlineUser(user);
        }    
        public void newGame(GameParams param)
        {
            Clients.All.gameCreated(param.Name, param.Players);
        }
        public void stopGame(string gameName)
        {
            Clients.All.gameStopped(gameName);
        }
        public void startGame(string name)
        {
            Clients.All.startGame(name);
        }
        public void gameFinished(string gameName)
        {
            GameService.stopGame(gameName);
        }
    }
}