﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNet.SignalR.Client;
using System.Threading;
using Service.Models;

namespace Service
{
    public static class GameService
    {
        static List<Player> siteUsers = new List<Player>();
        static List<GameParams> games = new List<GameParams>();        

        public static void createGame(GameParams param)
        {
            lobbyHub.Invoke("newGame", param);
        }

        public static OnlineUsersModel getUsersOnline()
        {
            var mdl = new OnlineUsersModel();
            mdl.Users = OnlineUsers;
            mdl.Games = games;            
            return mdl;
        }

        public static EntranceResult newPlayerEntered(string game,string player)
        {
           var gameCreated = games.Single(g => g.Name == game);
            if (gameCreated.State == GameState.Started) {                
                return EntranceResult.Full;
            }
            if (siteUsers.Any(u => u.Name == player && (DateTime.Now - u.LastLeaved < TimeSpan.FromMinutes(1))))
                return EntranceResult.Restricted;
            gameCreated.PlayersConnected++;

            if (gameCreated.Players == GameType.Players2 && gameCreated.PlayersConnected == 2
                || gameCreated.Players == GameType.Players4 && gameCreated.PlayersConnected == 4)
                return EntranceResult.Start;
            return EntranceResult.Wait;

        }

        public static void markPlayerLeaved(string user)
        {
            var usr = siteUsers.Single(p => p.Name == user);
            usr.LastLeaved = DateTime.Now;
        }

        public static void stopGame(string gameName)
        {
            games.Remove(games.Single(g => g.Name == gameName));              
            lobbyHub.Invoke("stopGame", gameName);
        }

        public static List<string> OnlineUsers { get
            {
                return siteUsers.Where(u => (DateTime.Now - u.LastOnline) < TimeSpan.FromSeconds(offlineTimeout)).Select(u => u.Name).ToList();
            } }
        static List<string> OfflineUsers
        {
            get
            {
                return siteUsers.Where(u => (DateTime.Now - u.LastOnline) > TimeSpan.FromSeconds(offlineTimeout)).Select(u => u.Name).ToList();
            }
        }

        static HubConnection connection = new HubConnection(ConfigurationManager.AppSettings["siteUrl"]);
        static IHubProxy lobbyHub;
        const int offlineTimeout = 10000;
        static object _mutex = new object();
        static GameService()
        {
            lobbyHub = connection.CreateHubProxy("LobbyHub");
            connection.Start().Wait(); 
            startOfflineKick();
        }
        static void startOfflineKick()
        {
            var waitHandle = new AutoResetEvent(false);
            ThreadPool.RegisterWaitForSingleObject(
                waitHandle,
                // Method to execute
                (state, timeout) =>
                {                   
                    foreach (var off in OfflineUsers)
                    {
                        lobbyHub.Invoke("Online", off, true);
                    }
    },
                // optional state object to pass to the method
                null,
                // Execute the method after 5 seconds
                TimeSpan.FromSeconds(30),
                // Set this to false to execute it repeatedly every 5 seconds
                false
            );
        }

        public static void onlineUser (string user)
        {
            lock(_mutex)
            {
                var usr = siteUsers.SingleOrDefault(u => u.Name == user);
                if (usr == null)
                {
                    siteUsers.Add(new Player { Name = user, LastOnline = DateTime.Now });
                    lobbyHub.Invoke("Online", user, false);
                }
                else {
                    //user returned
                    usr.LastOnline = DateTime.Now;
                }
            }
        }        
    }
}
