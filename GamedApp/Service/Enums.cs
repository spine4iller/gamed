﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service
{
    public enum GameType
    {
        Players2,
        Players4
    }
    public enum GameState
    {
        Waiting,
        Started        
    }
    public enum EntranceResult
    {
        Start,
        Full,
        Restricted,
        Wait
    }
}
