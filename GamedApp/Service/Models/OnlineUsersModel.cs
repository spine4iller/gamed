﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service.Models
{
    public class OnlineUsersModel
    {
        public List<string> Users { get; set; }
        public List<GameParams> Games { get; set; }        
    }

    
}
