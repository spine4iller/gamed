﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service.Models
{
    public class Player
    {
        public string Name { get; set; }
        public DateTime LastOnline { get; set; }
        public DateTime LastLeaved { get; set; }
    }
}
