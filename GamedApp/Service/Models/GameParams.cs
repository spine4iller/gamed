﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service.Models
{
    public class GameParams
    {
        public string Name { get; set; }
        public GameType Players { get; set; }

        public int PlayersConnected { get; set; }

        public GameState State { get; set; }
    }
    
}
